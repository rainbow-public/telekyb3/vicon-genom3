**Vicon component**

This is a genom3 component handling the communication with the Vicon tracker software.
It relies on the `Vicon DataStream SDK 1.8`. This SDK can be downloaded [here](https://www.vicon.com/downloads/utilities-and-sdk/datastream-sdk).

Output is compatible with the `pom` estimator (component from LAAS).

### Prerequisities

`Vicon DataStream SDK 1.11` is based on `boost` library that should be installed:

    $ sudo apt-get install libboost-all-dev
    
Get the `vicon-genom3` component source code in your workspace corresponding to `DRONE_WS` environment variable. Note that on `jetson` board `DRONE_WS=/home/nvidia/drone-ws`.

    $ export DRONE_WS=$HOME/drone-ws
    $ mkdir -p $DRONE_WS
    $ cd ~/drone-ws
    $ mkdir components; cd components
    $ git clone https://gitlab.inria.fr/lagadic/vicon-genom3.git
    
You need now to link vicon sdk library `libViconDataStreamSDK_CPP.so` to `/usr/lib`. Only one of the following cases should be considered.

- You are on Ubuntu 16.04 or 18.04 and want to use the pre-build library provided in `Vicon DataStream SDK 1.11` that is linked to `boost-*-mt.so` libraries also provided in the `Vicon DataStream SDK 1.8` 

	    $ cd /usr/lib
	    $ sudo ln -s ${DRONE_WS}/components/vicon-genom3/ViconDataStreamSDK/`lsb_release -rs`/Linux64/libViconDataStreamSDK_CPP.so .
	
- You are on Jetson TX2 and want to use the pre-build library that was internally build from `Vicon DataStream SDK 1.11` source code and from `boost` comming from sytem package `libboost-all-dev`

	    $ cd /usr/lib
	    $ sudo ln -s ${DRONE_WS}/components/vicon-genom3/ViconDataStreamSDK/`lsb_release -rs`/Linux-aarch64/libViconDataStreamSDK_CPP.so .

- You are on Odroid or Raspberry Pi 4 and want to use the pre-build library that was internally build from `Vicon DataStream SDK 1.11` source code and from `boost` comming from sytem package `libboost-all-dev`

	    $ cd /usr/lib
	    $ sudo ln -s ${DRONE_WS}/components/vicon-genom3/ViconDataStreamSDK/`lsb_release -rs`/Linux-armv7l/libViconDataStreamSDK_CPP.so .

- You are on Odroid and want to use the pre-build library that was internally built from `Vicon DataStream SDK 1.11` source code and from `boost` coming from sysyem package `libboost-all-dev`

	   $ cd /usr/lib
	   $ sudo ln -s ${DRONE_WS}/components/vicon-genom3/ViconDataStreamSDK/`lsb_release -rs`/Linux-armv8/libViconDataStreamSDK_CPP.so .

### Installation

Then install the component as followed :
    
    $ cd ${DRONE_WS}/components/vicon-genom3 && mkdir build
    $ autoreconf -vif
    $ cd codels/
    $ g++ -c SDK_wrapper.cpp -fPIC
    $ cd ../build
    $ ../configure --prefix=$DRONE_WS --with-templates=ros/server,ros/client/ros,ros/client/c,pocolibs/server,pocolibs/client/c
    $ make
    $ make -j4
    
If make throws this warning, `WARNING: 'asciidoctor' is missing on your system.` just run `$ sudo apt-get install asciidoctor`

#### Installation for `pocolibs` middleware (required)

    $ make install
    $ cd ${ROBOTPKG_BASE}/lib/genom/pocolibs/plugins
    $ ln -s ${DRONE_WS}/lib/genom/pocolibs/plugins/*.so .
    $ echo "export PATH=$PATH:${DRONE_WS}/bin" >> ~/.bashrc
    
#### Installation for `ROS` middleware (to avoid, since ROS should not be used on drones)

    $ make install
    $ cd ${ROBOTPKG_BASE}/lib/genom/ros/plugins
    $ ln -s ${DRONE_WS}/lib/genom/ros/plugins/*.so .

Launch the component with `vicon-ros`


### Known Issue

**Issue when configuring with `ROS`**
`rospkg` module not found. Should be `PYTHONPATH` problem.

After an update of genom3-ros, you may no longer be able to run the previously build component (the update overwrite the library).

The following error will show up :

    "libros-client-1.18.so: cannot open shared object file: No such file or directory
    /vicon: cannot open shared object file: No such file or directory
    /vicon.so: cannot open shared object file: No such file or directory

In this example, the last version of genom3-ros is '1.20' and th vicon components is looking to use one that is no longer used (1.18)

To fix this, you only need to redo the install procedure as describe above.
