/*
 * Copyright (c) 2017 IRISA/Inria
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 *				      Quentin Delamare on Mon Mar 13 2017
 */

#ifndef H_VICON_CODELS
#define H_VICON_CODELS

#include "acvicon.h"
#include "vicon_c_types.h"

#include "SDK_wrapper.h"
#include <stdio.h>

struct vicon_bodies_info_s {
  const struct tracker_descr *descr;
};

struct vicon_log_s {
  FILE *f;
};


genom_event	vicon_create_ports(const struct tracker_descr *descr,
                                   const vicon_ids_pubdata_s *pubdata,
                                   const vicon_bodies *bodies,
                                   const vicon_markers *markers,
                                   genom_context self);
genom_event	vicon_update_ports(const struct tracker_data *data,
                                   vicon_bodies_info_s *binfo,
                                   const vicon_ids_pubdata_s *pubdata,
                                   const vicon_ids_noise_s *noise,
                                   const vicon_bodies *bodies,
                                   const vicon_markers *markers,
                                   FILE *log, genom_context self);

#endif /* H_VICON_CODELS */

