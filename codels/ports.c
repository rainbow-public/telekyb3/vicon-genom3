/*
 * Copyright (c) 2017 IRISA/Inria
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 * 				      Quentin Delamare on Mon Mar 13 2017
 */
#include "acvicon.h"

#include <time.h>
#include <err.h>
#include <math.h>
#include <stdio.h>

#include "vicon_c_types.h"
#include "codels.h"


static double	gnoise(double stddev);


/* --- vicon_create_ports --------------------------------------------------- */

genom_event
vicon_create_ports(const struct tracker_descr *descr,
                   const vicon_ids_pubdata_s *pubdata,
                   const vicon_bodies *bodies,
                   const vicon_markers *markers,
                   genom_context self)
{
  const struct tracker_bodydescr *body;
  genom_event e;
  uint32_t i;

  for(i = 0, body = descr->body; i < descr->nbodies; i++, body++) {
    if (pubdata->bodies) {
      if (!bodies->data(body->safename, self)) {
        or_pose_estimator_state *body_data;

        e = bodies->open(body->safename, self);
        if (e != genom_ok) {
          warnx("failed to export body '%s'", body->safename);
          if (e == genom_syserr_id) {
            const genom_syserr_detail *d = self->raised(NULL, self);
            if (d && d->code == ENAMETOOLONG) {
              /* ignore long names */
            } else return e;
          } else return e;
        }
      }
    }

      if (pubdata->markers) {
        if (!markers->data(body->safename, self)) {
          e = markers->open(body->safename, self);
          if (e != genom_ok) {
            warnx("failed to export markers '%s'", body->safename);
            if (e == genom_syserr_id) {
              const genom_syserr_detail *d = self->raised(NULL, self);
              if (d && d->code == ENAMETOOLONG) {
                /* ignore long names */
              } else return e;
            } else return e;
          }
        }
      }
    }

  if (pubdata->anon && !markers->data("anonymous", self)) {
    e = markers->open("anonymous", self);
    if (e != genom_ok) {
      warnx("failed to export anonymous markers");
      return e;
    }
  }

  return genom_ok;
}


/* --- vicon_update_ports --------------------------------------------------- */

genom_event
vicon_update_ports(const struct tracker_data *data,
                  vicon_bodies_info_s *binfo,
                  const vicon_ids_pubdata_s *pubdata,
                  const vicon_ids_noise_s *noise,
                  const vicon_bodies *bodies,
                  const vicon_markers *markers,
                  FILE *log, genom_context self)
{
  //printf("update_ports: %d bodies \n", data->nbodies);///DEBUG
  const struct tracker_body *body;
  genom_event e;
  uint32_t i, j;

  for(i=0, body=data->body; i<data->nbodies; i++, body++) {
    if (pubdata->bodies) {
      or_pose_estimator_state *state =
        bodies->data(body->descr->safename, self);
      if (!state) /* ignore non-existing ports */ continue;
      
      /* frame timestamp */
      struct timeval tv; // TODO this shouldn't be done here...
      gettimeofday(&tv, NULL);

      state->ts.sec = tv.tv_sec;
      state->ts.nsec = tv.tv_usec * 1000;

      state->intrinsic = false;//false=global
      
      ////TODO: use 'occluded' to determine if pose is '_present'
      state->pos._present = 1;//(body->pose) && body->error > 0.);////????
      state->att._present = 1;
      state->pos_cov._present = state->pos._present;
      state->att_cov._present = state->att._present;

      if (state->pos._present) {
        double qw, qx, qy, qz;
        double s;
        state->pos._value.x = body->pose.x;
        state->pos._value.y = body->pose.y;
        state->pos._value.z = body->pose.z;

        qw = state->att._value.qw = body->pose.qw;
        qx = state->att._value.qx = body->pose.qx;
        qy = state->att._value.qy = body->pose.qy;
        qz = state->att._value.qz = body->pose.qz;

        /* add gaussian noise if requested */
        if (noise->pstddev > 0.) {
          state->pos._value.x += gnoise(noise->pstddev);
          state->pos._value.y += gnoise(noise->pstddev);
          state->pos._value.z += gnoise(noise->pstddev);
        }
        if (noise->qstddev > 0.) {
          double nw;
          double nx = gnoise(noise->qstddev);
          double ny = gnoise(noise->qstddev);
          double nz = gnoise(noise->qstddev);
          double n = sqrt(nx*nx + ny*ny + nz*nz);
          if (n > 0.) {
            nw = cos(n/2);
            nx *= sin(n/2)/n;
            ny *= sin(n/2)/n;
            nz *= sin(n/2)/n;

            state->att._value.qw = qw*nw - qx*nx - qy*ny - qz*nz;
            state->att._value.qx = qw*nx + qx*nw + qy*nz + qz*ny;
            state->att._value.qy = qw*ny - qx*nz + qy*nw + qz*nx;
            state->att._value.qz = qw*nz + qx*ny - qy*nx + qz*nw;
          }
        }

        /* provide fake but hopefully realistic covariance */
        double body_error = 0.0005; // TODO: compute a realistic error from vicon data (how ??)
        s = 10. * body_error * body_error;
        s += noise->pstddev;
        state->pos_cov._value.cov[0] = s;
        state->pos_cov._value.cov[1] = 0.;
        state->pos_cov._value.cov[2] = s;
        state->pos_cov._value.cov[3] = 0.;
        state->pos_cov._value.cov[4] = 0.;
        state->pos_cov._value.cov[5] = s;

        s = 100. * body_error * body_error;
        s += noise->qstddev;
        state->att_cov._value.cov[0]  = s * (1 - qw*qw);
        state->att_cov._value.cov[1] = s * -qw*qx;
        state->att_cov._value.cov[2] = s * (1 - qx*qx);
        state->att_cov._value.cov[3] = s * qw*qy;
        state->att_cov._value.cov[4] = s * -qx*qy;
        state->att_cov._value.cov[5] = s * (1 - qy*qy);
        state->att_cov._value.cov[6] = s * -qw*qz;
        state->att_cov._value.cov[7] = s * -qx*qz;
        state->att_cov._value.cov[8] = s * -qy*qz;
        state->att_cov._value.cov[9] = s * (1 - qz*qz);
      }

      e = bodies->write(body->descr->safename, self);
      if (e != genom_ok) {
        warnx("body %s cannot be published", body->descr->safename);
        return e;
      }

      if (log && state->pos._present) {
        double qw = state->att._value.qw;
        double qx = state->att._value.qx;
        double qy = state->att._value.qy;
        double qz = state->att._value.qz;
        double roll, pitch, yaw;

        roll  = atan2(2 * (qw*qx + qy*qz), 1 - 2 * (qx*qx + qy*qy));
        pitch = asin(2 * (qw*qy - qz*qx));
        yaw   = atan2(2 * (qw*qz + qx*qy), 1 - 2 * (qy*qy + qz*qz));

        fprintf(log, "%s %d.%09d %f   %g %g %g   %g %g %g   %g\n",
                body->descr->safename,
                state->ts.sec, state->ts.nsec,
                state->pos._value.x,
                state->pos._value.y,
                state->pos._value.z,
                roll, pitch, yaw,
                //body->error,
                tv.tv_sec - state->ts.sec +
                (tv.tv_usec - state->ts.nsec/1000.) * 1e-6);
      }
    }

    if (pubdata->markers) {
      uint32_t i;
      vicon_marker_set *state =
        markers->data(body->descr->safename, self);
      if (!state) /* ignore non-existing ports */ continue;
//      if (!body->pose || body->error == 0.) /* not tracked */ continue;

      /* frame timestamp */
      struct timeval tv; // TODO this shouldn't be done here...
      gettimeofday(&tv, NULL);

      state->ts.sec = tv.tv_sec;
      state->ts.nsec = tv.tv_usec * 1000;
 
      /* markers sequence */
      if (state->markers._maximum != body->mset.n)
        if (genom_sequence_reserve(&state->markers, body->mset.n))
          return vicon_e_nomem(self);

      for(i = 0; i < body->mset.n; i++) {
        state->markers._buffer[i].x = body->mset.pos[i].x;
        state->markers._buffer[i].y = body->mset.pos[i].y;
        state->markers._buffer[i].z = body->mset.pos[i].z;
      }
      state->markers._length = body->mset.n;

      e = markers->write(body->descr->safename, self);
      if (e != genom_ok) {
        warnx("markers %s cannot be published", body->descr->safename);
        return e;
      }

      if (log) {
        for(i = 0; i < body->mset.n; i++) {
          fprintf(log, "%s:%d %d %d   %g %g %g\n",
                  body->descr->safename, i,
                  state->ts.sec, state->ts.nsec,
                  state->markers._buffer[i].x,
                  state->markers._buffer[i].y,
                  state->markers._buffer[i].z);
        }
      }
    }
  }

  if (pubdata->anon) {
    uint32_t i;
    vicon_marker_set *state = markers->data("anonymous", self);
    if (!state) {
      warnx("anonymous markers cannot be published");
      return vicon_e_publish(self);
    }
    /* frame timestamp */
    struct timeval tv; // TODO this shouldn't be done here...
    gettimeofday(&tv, NULL);

    state->ts.sec = tv.tv_sec;
    state->ts.nsec = tv.tv_usec * 1000;
 
    /* markers sequence */
    if (state->markers._maximum < data->aset.n)
      if (genom_sequence_reserve(&state->markers, data->aset.n))
        return vicon_e_nomem(self);

    for(i = 0; i < data->aset.n; i++) {
      state->markers._buffer[i].x = data->aset.pos[i].x;
      state->markers._buffer[i].y = data->aset.pos[i].y;
      state->markers._buffer[i].z = data->aset.pos[i].z;
    }
    state->markers._length = data->aset.n;

    e = markers->write("anonymous", self);
    if (e != genom_ok) {
      warnx("anonymous markers cannot be published");
      return e;
    }
    if (log) {
      for(i = 0; i < data->aset.n; i++) {
        fprintf(log, "anonymous:%d %d %d   %g %g %g\n",
                i,
                state->ts.sec, state->ts.nsec,
                state->markers._buffer[i].x,
                state->markers._buffer[i].y,
                state->markers._buffer[i].z);
      }
    }
  }

  return genom_ok;
}


/* --- gnoise -------------------------------------------------------------- */

static double
gnoise(double stddev)
{
  double u1, u2;
  double s;

  do {
    u1 = 2 * drand48() - 1; /* [-1; 1] */
    u2 = 2 * drand48() - 1;

    s = u1*u1 + u2*u2;
  } while(s >= 1.);

  return sqrt(-2. * log(s)/s * stddev) * u1;
}
