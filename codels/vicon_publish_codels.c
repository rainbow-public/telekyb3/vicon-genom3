/*
 * Copyright (c) 2017 IRISA/Inria
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 * 				      Quentin Delamare on Mon Mar 13 2017
 *                       based on optitrack component from Anthony Mallet
 */
#include "acvicon.h"

#include "SDK_wrapper.h"
#include "codels.h"

#include "vicon_c_types.h"

#define min(a, b) (a < b ? a : b)

static size_t strnfilter(char *dst, size_t n, const char *src,
                           const char *accept, char repl);

/* --- Task publish ----------------------------------------------------- */


/** Codel vicon_publish_start of task publish.
 *
 * Triggered by vicon_start.
 * Yields to vicon_pause_descr.
 * Throws vicon_e_nomem, vicon_e_sdk.
 */
genom_event
vicon_publish_start(vicon_ids *ids, const genom_context self)
{
  printf("entering publish_start\n");///DEBUG

  ids->pubdata.bodies = true;
  ids->pubdata.markers = false;
  ids->pubdata.anon = false;

  ids->binfo = calloc(1, sizeof(ids->binfo));
  if (!ids->binfo) return vicon_e_nomem(self);

  ids->noise.pstddev = 0.;
  ids->noise.qstddev = 0.;

  return vicon_pause_descr;
}


/** Codel vicon_publish_descr of task publish.
 *
 * Triggered by vicon_descr.
 * Yields to vicon_pause_descr, vicon_sdkrecv, vicon_taskdisconnect.
 * Throws vicon_e_nomem, vicon_e_sdk.
 */
genom_event
vicon_publish_descr(vicon_bodies_info_s **binfo,
                    const vicon_ids_pubdata_s *pubdata,
                    const vicon_bodies *bodies,
                    const vicon_markers *markers,
                    const genom_context self)
{

  //printf("entering publish_descr\n");///DEBUG

  // ask vicon sdk for descsription of bodies & markers (counts and names)
  unsigned int n_subj, k,i;
  unsigned int n_mkr;
  char name[64], rsname[64], mname[64];
  vicon_e_sdk_detail e_sdk;
  size_t e_sdk_size=sizeof(e_sdk.what);
  struct tracker_descr *d;

  // if not already done, wait for connection (triggered in activity vicon_connection_start)
  if( !viconsdk_isConnected()) return (genom_event) vicon_pause_descr;

  //request a frame of data
  if( viconsdk_getFrame(e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }
  if( (*binfo)->descr ) { //descriptor already exists
    d = (struct tracker_descr*) (*binfo)->descr; // get it
  }
  else
  {
    d = calloc(1, sizeof(*d));
  }

  // get the number of subjects
  if( viconsdk_getSubjectCount(&n_subj, e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }

  if(n_subj < d->nbodies) { // there's less subjects than lats iteration, so let's free the memory not used anymore
    /*for(k=n_subj;k<d->nbodies;k++) {
      free(d->body[k].name);//this will not be used anymore
      free(d->body[k].rsname);
      free(d->mset[k].name);
      for(i=0;i<d->mset[k].nmnames;i++) {
        free(d->mset[k].lmname[i]);//marker name i of mset k
      }
      free(d->mset[k].lmname);//free the table of char*
      printf("freed pointers for body %d/%d\n", k, d->nbodies);///DEBUG
    }
    ////for some reason this makes the soft crash when happening (ie when an object disappears) !
    ////I don't understand why for now...
    ////maybe check that k is in the right range ? */
  } else if(n_subj > d->nbodies) {
    //there are new guys, let's extend memory space
    d->body = realloc(d->body, n_subj*sizeof(*d->body));//pointer to [tracker_bodydescr 1..n]
    d->mset = realloc(d->mset, n_subj*sizeof(*d->mset));
    if((!d->mset) || (!d->body)) { printf("publish_descr: alloc fail for the body/mset descriptor list\n"); return (genom_event) vicon_e_nomem; }
  }

  //printf("publish_descr: %d subjects\n",n_subj);///DEBUG
  
  // for each one, get its name and its markers
  for(k=0;k<min(n_subj,d->nbodies);k++) { // this loop only updates the list we already had before
    if( viconsdk_getSubjectName(k, name, sizeof(name), e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }
    if( viconsdk_getSubjectRootSegmentName(name, rsname, sizeof(rsname), e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }
    if( viconsdk_getMarkerCount(name, &n_mkr, e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }

    d->body[k].name = realloc(d->body[k].name, sizeof(name)*sizeof(char));//re-use mem from last descriptor
    d->body[k].rsname = realloc(d->body[k].rsname, sizeof(rsname)*sizeof(char));
    if((!d->body[k].name) || (!d->body[k].rsname) ) { printf("publish_descr: alloc fail for body %d\n", k); return (genom_event) vicon_e_nomem; }

    if(n_mkr < d->mset[k].nmnames) { // there are less markers than before for this mset
      for(i=n_mkr;i<d->mset[k].nmnames;i++) { // so let's free corresponding names
        free(d->mset[k].lmname[i]);
      }
      //d->mset[k].lmname = realloc(d->mset[k].lmname, n_mkr*sizeof(char*));
      ///we could reallocate a smaller list but this is not required (last pointers will just not be used till necessary)
    } else if(n_mkr > d->mset[k].nmnames) {
      d->mset[k].lmname = realloc(d->mset[k].lmname, n_mkr*sizeof(char*));
    }
    
    if((!d->mset[k].lmname) || (!d->body[k].name) || (!d->body[k].rsname) ) { printf("publish_descr: alloc fail for mset %d\n", k); return (genom_event) vicon_e_nomem; }

    strncpy(d->body[k].name, name, sizeof(name));//store name
    strncpy(d->body[k].rsname, rsname, sizeof(rsname));//store name

    strnfilter(d->body[k].safename, sizeof(d->body[k].safename), name, "[a-zA-Z0-9_]", '_');
    strnfilter(d->body[k].rssafename, sizeof(d->body[k].rssafename), rsname, "[a-zA-Z0-9_]", '_');
    d->mset[k].name = d->body[k].name; // use the same data
    strncpy(d->mset[k].safename, d->body[k].safename, sizeof(d->body[k].safename));//copy...

    ///now retrieve each marker name
    for(i=0;i<min(d->mset[k].nmnames, n_mkr);i++) { // this loop updates makers we already had
      if( viconsdk_getMarkerName(name, i, mname, sizeof(mname), e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }
      d->mset[k].lmname[i] = realloc(d->mset[k].lmname[i], sizeof(mname)*sizeof(char)); // re-use last memory up to the size we had before
      strncpy(d->mset[k].lmname[i], mname, sizeof(mname));//store name
    }
    for(i=min(d->mset[k].nmnames, n_mkr);i<n_mkr;i++) { // this one is for the new markers
      if( viconsdk_getMarkerName(name, i, mname, sizeof(mname), e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }
      d->mset[k].lmname[i] = calloc(sizeof(mname), sizeof(char)); // first allocation for the new space
      if(!d->mset[k].lmname[i]) { printf("publish_descr: alloc fail for marker name %d of mset %d\n", i, k); return (genom_event) vicon_e_nomem; }
      strncpy(d->mset[k].lmname[i], mname, sizeof(mname));//store name
    }

    d->mset[k].nmnames = n_mkr;
    //printf("publish_descr: '%s' has %d markers\n",name,n_mkr);///DEBUG
  }

  for(k=min(n_subj,d->nbodies);k<n_subj;k++) { ///now do the same for new subjects
    if( viconsdk_getSubjectName(k, name, sizeof(name), e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }
    if( viconsdk_getSubjectRootSegmentName(name, rsname, sizeof(rsname), e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }
    if( viconsdk_getMarkerCount(name, &n_mkr, e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }
    d->body[k].name = calloc(sizeof(name), sizeof(char));//first allocation
    d->body[k].rsname = calloc(sizeof(rsname), sizeof(char));
    d->mset[k].lmname = calloc(n_mkr, sizeof(char*));
    if((!d->mset[k].lmname) || (!d->body[k].name) || (!d->body[k].rsname) ) { printf("publish_descr: new subject names alloc fail\n"); return (genom_event) vicon_e_nomem; }

    strncpy(d->body[k].name, name, sizeof(name));//store name
    strncpy(d->body[k].rsname, rsname, sizeof(rsname));//store rsname

    strnfilter(d->body[k].safename, sizeof(d->body[k].safename), name, "[a-zA-Z0-9_]", '_');
    strnfilter(d->body[k].rssafename, sizeof(d->body[k].rssafename), rsname, "[a-zA-Z0-9_]", '_');
    d->mset[k].name = d->body[k].name; // use the same data
    strncpy(d->mset[k].safename, d->body[k].safename, sizeof(d->body[k].safename));//copy...

    ///now retrieve each marker name
    for(i=0;i<n_mkr;i++) {
      if( viconsdk_getMarkerName(name, i, mname, sizeof(mname), e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }
      d->mset[k].lmname[i] = calloc(sizeof(mname), sizeof(char)); // first allocation for the new space
      if(!d->mset[k].lmname[i]) { printf("publish_descr: alloc fail for marker name %d of new mset %d\n", i, k); return (genom_event) vicon_e_nomem; }
      strncpy(d->mset[k].lmname[i], mname, sizeof(mname));//store name
    }

    d->mset[k].nmnames = n_mkr;
    //printf("publish_descr: new subject '%s' has %d markers\n",name,n_mkr);///DEBUG
  }

  d->nbodies = n_subj;
  d->nmsets = n_subj;

  (*binfo)->descr = d; // push back descriptor (pointer)

  if (vicon_create_ports((*binfo)->descr, pubdata, bodies, markers, self) != genom_ok) return (genom_event) vicon_taskdisconnect;

  return (genom_event) vicon_sdkrecv; // now goto state where data is handled
}

void free_data(struct tracker_data data) {
  ///simply free data structure pointers
  //called when leaving sdkrecv before its end
  int k;
  for(k=0;k<data.nmsets;k++) {
    free(data.mset[k].mset.pos);
  }
  free(data.mset);
  free(data.body);

  free(data.aset.pos);
}

/** Codel vicon_publish_sdkrecv of task publish.
 *
 * Triggered by vicon_sdkrecv.
 * Yields to vicon_descr, vicon_sdkrecv.
 * Throws vicon_e_nomem, vicon_e_sdk.
 */
genom_event
vicon_publish_sdkrecv(vicon_bodies_info_s **binfo,
                      const vicon_log_s *log,
                      const vicon_ids_pubdata_s *pubdata,
                      const vicon_ids_noise_s *noise,
                      const vicon_bodies *bodies,
                      const vicon_markers *markers,
                      const genom_context self)
{
  //printf("entering publish_sdkrecv\n");///DEBUG

  unsigned int n_subj, n_anon;
  vicon_e_sdk_detail e_sdk;
  size_t e_sdk_size = sizeof(e_sdk.what);
  char *name, *rsname;
  double x,y,z,qw,qx,qy,qz;
  int occluded;
  struct tracker_data data;
  int k, i;
  
  //request a frame of data
  if( viconsdk_getFrame(e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }

  //check subject count
  if( viconsdk_getSubjectCount(&n_subj, e_sdk.what, e_sdk_size) ) { return (genom_event) vicon_e_sdk(&e_sdk, self); }

  // Check markers count
  if(viconsdk_getUnlabeledMarkerCount(&n_anon, e_sdk.what, e_sdk_size)) { return (genom_event) vicon_e_sdk(&e_sdk, self); }

  data.aset.pos = malloc(n_anon * 3 * sizeof(float));

  if( !(*binfo) || !(*binfo)->descr || (*binfo)->descr->nbodies != n_subj ) {
    //printf("publish_sdkrecv: the number of subjects has changed\n");///DEBUG
    return (genom_event) vicon_descr; // something has changed : refetch descr
  }

  // local data structure
  data.mset = calloc((*binfo)->descr->nmsets, sizeof(*data.mset));
  data.body = calloc((*binfo)->descr->nbodies, sizeof(*data.body));

  if((!data.mset) || (!data.body) ) {
    printf("publish_sdkrecv: alloc fail for data structure\n");
    free_data(data);
    return (genom_event) vicon_e_nomem;
  }

  //for each subject, retrieve pose and markers pos
  for(k=0;k<n_subj;k++) 
  {
    name = (*binfo)->descr->body[k].name;
    rsname = (*binfo)->descr->body[k].rsname;

    if( viconsdk_getSegmentPos(name, rsname, &x, &y, &z, &occluded, e_sdk.what, e_sdk_size) ) { 
      //printf("publish_sdkrecv: getSegmentPos %s \n", e_sdk.what);///DEBUG
      free_data(data);
      return (genom_event) vicon_descr; /*vicon_e_sdk(&e_sdk, self);*/   /// if something's wrong, try to refetch descr
    }
    if(occluded) { warnx("publish_sdkrecv: '%s' is occluded !! (the component doesn't handle this situation for now 04-17)\n", name); }///TODO what do we do when something's occluded ?

    if( viconsdk_getSegmentQuat(name, rsname, &qw, &qx, &qy, &qz, &occluded, e_sdk.what, e_sdk_size) ) {
      //printf("publish_sdkrecv: getSegmentQuat %s \n", e_sdk.what);///DEBUG
      free_data(data);
      return (genom_event) vicon_descr; /*vicon_e_sdk(&e_sdk, self);*/   /// if something's wrong, try to refetch descr
    }
    data.body[k].descr = &((*binfo)->descr->body[k]);
    data.body[k].pose.x = x*0.001; data.body[k].pose.y = y*0.001; data.body[k].pose.z = z*0.001;
    data.body[k].pose.qw=qw; data.body[k].pose.qx=qx;
    data.body[k].pose.qy=qy; data.body[k].pose.qz=qz;

    data.mset[k].descr = &((*binfo)->descr->mset[k]);
    data.mset[k].name = (*binfo)->descr->mset[k].name;
    data.mset[k].mset.n = (*binfo)->descr->mset[k].nmnames;
    data.mset[k].mset.pos = calloc(data.mset[k].mset.n, sizeof(*data.mset[k].mset.pos));

    if(!data.mset[k].mset.pos) {
      printf("publish_sdkrecv: alloc fail for mset[%d].pos\n", k);
      free_data(data);
      return (genom_event) vicon_e_nomem;
    }

    for(i=0;i<data.mset[k].mset.n;i++) {
      if( viconsdk_getMarkerPos(name, data.mset[k].descr->lmname[i], &x, &y, &z, &occluded, e_sdk.what, e_sdk_size) ) {
        //printf("publish_sdkrecv: getMarkerPos %s \n", e_sdk.what);///DEBUG
        return (genom_event) vicon_descr; /*vicon_e_sdk(&e_sdk, self);*/   /// if something's wrong, try to refetch descr
      }
      data.mset[k].mset.pos[i].x = x*0.001;
      data.mset[k].mset.pos[i].y = y*0.001;
      data.mset[k].mset.pos[i].z = z*0.001;
      //TODO: also output occluded state (?)
    }
    data.body[k].mset = data.mset[k].mset;///???
  }

  //TODO:anonymous markers 
  // printf("%d\n",n_anon);
  for(i=0; i < n_anon; i++)
  {
    // printf("%d\n",i);

    if( viconsdk_getUnlabeledMarkerPos(i, &x, &y, &z, e_sdk.what, e_sdk_size))
    {
      free_data(data);
      return (genom_event) vicon_descr;
    }

    data.aset.pos[i].x = x*0.001;
    data.aset.pos[i].y = y*0.001;
    data.aset.pos[i].z = z*0.001;
  }

  data.nmsets = n_subj;
  data.nbodies= n_subj;
  data.aset.n = n_anon;

  // output data
  vicon_update_ports(&data, *binfo, pubdata, noise, bodies, markers, log?log->f:NULL, self);

  //data have been copied inside update_ports, so free it now
  free_data(data);

  return (genom_event) vicon_sdkrecv;
}


/** Codel vicon_publish_taskdisconnect of task publish.
 *
 * Triggered by vicon_taskdisconnect.
 * Yields to vicon_pause_descr.
 * Throws vicon_e_nomem, vicon_e_sdk.
 */
genom_event
vicon_publish_taskdisconnect(const genom_context self)
{
  vicon_e_sdk_detail e_sdk;
  size_t e_sdk_size=sizeof(e_sdk.what);

  if(viconsdk_disconnect(e_sdk.what, e_sdk_size)) {
    return (genom_event) vicon_e_sdk(&e_sdk, self);
  }

  printf("now disconnected\n");///DEBUG

  return (genom_event) vicon_pause_descr;
}


/** Codel vicon_publish_stop of task publish.
 *
 * Triggered by vicon_stop.
 * Yields to vicon_ether.
 * Throws vicon_e_nomem, vicon_e_sdk.
 */
genom_event
vicon_publish_stop(vicon_bodies_info_s **binfo,
                   const genom_context self)
{
  int k, i;

  vicon_publish_taskdisconnect(self);

  if(binfo && *binfo) {
    /*if((*binfo)->descr) {
      for(k=0;k<(*binfo)->descr->nbodies;k++) {
        free((*binfo)->descr->body[k].name);
        free((*binfo)->descr->body[k].rsname);
        free((*binfo)->descr->mset[k].name);
        for(i=0;i<(*binfo)->descr->mset[k].nmnames;i++) {
          free((*binfo)->descr->mset[k].lmname[i]);//marker name i of mset k
        }
        free((*binfo)->descr->mset[k].lmname);
      }
      free((*binfo)->descr->body);
      free((*binfo)->descr->mset);
      free((void*) (*binfo)->descr);
    }*/
    free(*binfo);
  }

  return (genom_event) vicon_ether;
}


/* --- Activity ping ---------------------------------------------------- */

/** Codel vicon_ping_start of activity ping.
 *
 * Triggered by vicon_start.
 * Yields to vicon_send.
 * Throws vicon_e_nomem, vicon_e_sdk.
 */
genom_event
vicon_ping_start(const vicon_ids_ping_ctx *ping, int32_t *id,
                 double *ts, int32_t *retries,
                 const genom_context self)
{
  warnx("ping is not yet implemented ! nothing will happen");
  return vicon_send;
}

/** Codel vicon_ping_send of activity ping.
 *
 * Triggered by vicon_send.
 * Yields to vicon_pause_sleep.
 * Throws vicon_e_nomem, vicon_e_sdk.
 */
genom_event
vicon_ping_send(const genom_context self)
{
  /* skeleton sample: insert your code */
  /* skeleton sample */ return vicon_pause_sleep;
}

/** Codel vicon_ping_sleep of activity ping.
 *
 * Triggered by vicon_sleep.
 * Yields to vicon_send, vicon_stop.
 * Throws vicon_e_nomem, vicon_e_sdk.
 */
genom_event
vicon_ping_sleep(const vicon_ids_ping_ctx *ping, int32_t id,
                 int32_t *retries, const genom_context self)
{
  /* skeleton sample: insert your code */
  /* skeleton sample */ return vicon_send;
}

/** Codel vicon_ping_stop of activity ping.
 *
 * Triggered by vicon_stop.
 * Yields to vicon_ether.
 * Throws vicon_e_nomem, vicon_e_sdk.
 */
genom_event
vicon_ping_stop(const vicon_ids_ping_ctx *ping, double ts, double *rtt,
                char info[64], const genom_context self)
{
  /* skeleton sample: insert your code */
  /* skeleton sample */ return vicon_ether;
}


/* --- Activity connect ------------------------------------------------- */

/** Codel vicon_connect_start of activity connect.
 *
 * Triggered by vicon_start.
 * Yields to vicon_ether.
 * Throws vicon_e_nomem, vicon_e_sdk.
 */
genom_event
vicon_connect_start(const char host[128], const genom_context self)
{

  printf("entering connect_start\n");///DEBUG

  vicon_e_sdk_detail e_sdk;
  size_t e_sdk_size=sizeof(e_sdk.what);

  if(viconsdk_isConnected()) {
    printf("connect_start: Vicon client is already connected !");
    return (genom_event) vicon_ether;
  }

  if(viconsdk_connect(host, e_sdk.what, e_sdk_size)) {
    return (genom_event) vicon_e_sdk(&e_sdk, self);
  }

  if(!viconsdk_isConnected()) {
    snprintf(e_sdk.what, e_sdk_size, "connect_start: Vicon client is not connected !");//shouldn't occur...
    return (genom_event) vicon_e_sdk(&e_sdk, self);
  }

  //try to set mode to serverpush (the quickest, otherwise it could have been clientpull or clientpullprefetch see vicon sdk doc)
  if(viconsdk_setStreamModeToServerPush(e_sdk.what, e_sdk_size)) {
    return (genom_event) vicon_e_sdk(&e_sdk, self);
  }

  //enable transmission of all kind of data
  if(!viconsdk_isSegmentDataEnabled()) {
    if(viconsdk_enableSegmentData(1, e_sdk.what, e_sdk_size)) {
      return (genom_event) vicon_e_sdk(&e_sdk, self);
    }
  }

  if(!viconsdk_isMarkerDataEnabled()) {
    if(viconsdk_enableMarkerData(1, e_sdk.what, e_sdk_size)) {
      return (genom_event) vicon_e_sdk(&e_sdk, self);
    }
  }

  if(!viconsdk_isUnlabeledMarkerDataEnabled()) {
    if(viconsdk_enableUnlabeledMarkerData(1, e_sdk.what, e_sdk_size)) {
      return (genom_event) vicon_e_sdk(&e_sdk, self);
    }
  }

  printf("connect_start: connection and initialisation succeeded\n");///DEBUG

  return (genom_event) vicon_ether;
}


/* --- Activity disconnect ---------------------------------------------- */

/** Codel vicon_disconnect of activity disconnect.
 *
 * Triggered by vicon_start.
 * Yields to vicon_ether.
 * Throws vicon_e_nomem, vicon_e_sdk.
 */
genom_event
vicon_disconnect(const genom_context self)
{
  vicon_publish_taskdisconnect(self);
  return (genom_event) vicon_ether;
}


/* --- strnfilter ---------------------------------------------------------- */

static size_t
strnfilter(char *dst, size_t n, const char *src, const char *accept, char repl)
{
  char x[2];

  x[1] = '\0';
  while(--n && *src) {
    x[0] = *src++;
    if (fnmatch(accept, x, 0))
      *dst++ = repl;
    else
      *dst++ = x[0];
  }
  *dst = '\0';
  return n;
}


