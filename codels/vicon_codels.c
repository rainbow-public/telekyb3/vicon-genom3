/*
 * Copyright (c) 2017 IRISA/Inria
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 * 				      Quentin Delamare on Mon Mar 13 2017
 *                       based on optitrack component from Anthony Mallet
 */
#include "acvicon.h"

#include <time.h>
#include "codels.h"

#include "vicon_c_types.h"


/* --- Attribute set_export --------------------------------------------- */

/** Validation codel set_export_validate of attribute set_export.
 *
 * Returns genom_ok.
 * Throws .
 */
genom_event
set_export_validate(vicon_bodies_info_s **binfo,
                    const genom_context self)
{
  if (binfo && *binfo) (*binfo)->descr = NULL;///?
  return genom_ok;
}

#ifndef PATH_MAX
#define PATH_MAX  1024  //should be defined !!??
#endif

/* --- Attribute set_logfile -------------------------------------------- */

/** Validation codel set_logfile of attribute set_logfile.
 *
 * Returns genom_ok.
 * Throws vicon_e_access.
 */
genom_event
set_logfile(const char logfile[256], vicon_log_s **log,
            const genom_context self)
{
vicon_e_access_detail d;
  char path[PATH_MAX];
  genom_event e;
  struct tm tm;
  time_t t;
  size_t s;
  FILE *f;

  t = time(NULL);
  localtime_r(&t, &tm);

  s = strftime(path, sizeof(path), logfile, &tm);
  if (s == 0 || s >= sizeof(path)-1) {
    snprintf(d.what, sizeof(d.what), "file name too long");
    return vicon_e_access(&d, self);
  }

  f = fopen(path, "w");
  if (!f) {
    snprintf(d.what, sizeof(d.what), "%s", strerror(errno));
    return vicon_e_access(&d, self);
  }
  fprintf(f, "name ts   x y z   roll pitch yaw  err delay\n");

  e = unset_logfile(log, self);
  if (e != genom_ok) return e;

  *log = malloc(sizeof(**log));
  if (!*log) return vicon_e_nomem(self);
  (*log)->f = f;
  
  return genom_ok;
}


/* --- Function refresh ------------------------------------------------- */

/** Codel refresh of function refresh.
 *
 * Returns genom_ok.
 * Throws vicon_e_sdk.
 */
genom_event
refresh(vicon_bodies_info_s **binfo, const genom_context self)
{
  (*binfo)->descr = NULL; /* refetch descriptions */
  return genom_ok;
}


/* --- Function body_list ----------------------------------------------- */

/** Codel vicon_body_list of function body_list.
 *
 * Returns genom_ok.
 * Throws vicon_e_nomem.
 */
genom_event
vicon_body_list(const vicon_bodies_info_s *binfo,
                sequence_string64 *body_list,
                const genom_context self)
{
  const struct tracker_descr *d;//////
  uint32_t i;

  if (!binfo->descr) {
    body_list->_length = 0;
    return genom_ok;
  }
  d = binfo->descr;

  if (genom_sequence_reserve(body_list, d->nbodies))
    return vicon_e_nomem(self);

  for(i = 0; i < d->nbodies; i++) {
    strncpy(body_list->_buffer[i], d->body[i].safename,
            sizeof(body_list->_buffer[i]));
  }
  body_list->_length = d->nbodies;  
  
  return genom_ok;
}


/* --- Function unset_logfile ------------------------------------------- */

/** Codel unset_logfile of function unset_logfile.
 *
 * Returns genom_ok.
 */
genom_event
unset_logfile(vicon_log_s **log, const genom_context self)
{
  if (!log || !*log) return genom_ok;
  if ((*log)->f) fclose((*log)->f);
  free(*log);
  *log = NULL;
  return genom_ok;
}
